# Addendum til forelesning 25. januar 2022

Vi hadde et lite problem på slutten av forelesning når vi skulle implementere *.hashCode* og *.equals*. Problemet vårt var at *signaturen* til *.equals* -metoden vår ikke var den som blir brukt av HashSet. Vi hadde følgende metode:

```java
public boolean equals(Sensor that) {
    return this.ip.equals(that.ip);
}
```
som har signaturen `equals(Sensor)`. Når biblioteks-koden for *.contains* i HashSet kaller *.equals*, ser den etter en signatur `equals(Object)`. Vi har ikke lært om arv enda, men *Object* er en spesiell klasse som alle andre klasser "arver." Det er for eksempel i klassen *Object* at den standard *.equals* -metoden er implementert, som nettopp har signaturen `equals(Object)`. Derfor ble ikke vår equals-metode brukt til å sammenligne, men istedet ble standard-implementasjonen som vi hadde arvet fra *Object* brukt. Denne standard -implementasjonen i java sitt bibliotek ser nøyaktig slik ut:
```java
public boolean equals(Object obj) {
        return (this == obj);
    }
```
Dermed ble objektene våre bedømt å være ulike, ingen feilmelding ble gitt, og begge sensor-objektene ble installert (i strid med vår intensjon).

## Løsningen

Løsningen er å implementere *.equals* -metoden med riktig signatur:
```java
public boolean equals(Object obj) {
    if (!(obj instanceof Sensor)) {
        return false;
    }
    Sensor that = (Sensor) obj;
    return this.ip.equals(that.ip);
}
```

Fordi parameteret nå er et objekt av typen *Object*, sjekker vi først om objektet vi får egentlig er av typen *Sensor*. Hvis det ikke er det, er ikke objektene like (vi vet allerede at *this* er en Sensor, siden vi skriver inni Sensor-klassen). Vi kan nå trygt "caste" objektet til typen Sensor. Så gjør vi sjekken vi gjorde før.

## Automatisk generert hashCode og equals

VS Code -brukere: Søk etter *java code generators* i extensions. I Eclipse er dette innebygget. I en klasse, høyreklikk og velg "generate hash-code and equals" eller lignende (i VS Code ligger det under Java Code Generators i høyreklikkmenyen).


## @Override
Det er god praksis å annotere metoder man "overskriver" med `@Override`. Dette har ingen funksjon for selve kompilering og kjøringen, men det hjelper din IDE med å finne (sannsynlige) feil, og hjelper deg selv til å holde styr på hva som opprinnelig kommer fra din klasse og hva du overstyrer av oppførsel du har arvet. Denne fotnoten vil gi mer mening etterhvert som vi lærer om arv.